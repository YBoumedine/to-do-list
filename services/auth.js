const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const User = require('../models/User')


dotenv.config()

var authService = {
    verify: async(token) =>{
        return jwt.verify(token, process.env.SECRET);
    },
    signup: async(_email,_username,password) =>{
        const user = await User.findOne({ $or:[{email:_email}, {username:_username}] });
        if(!user){
            let newuser = new User({
                id: new mongoose.Types.ObjectId(),
                email: _email,
                username: _username,
                hashPW:'',
                salt:'',
                todos:[]
            })
            await newuser.setPassword(password);                
            await newuser.save(function(err){
             if(err) throw err;
            })
            return {success:true, value:'User created successfuly.'}
        }else{
            return { success:false, value:'Username or Email already used.'}
        }
    },

    login: async(username, password) => {
        let jsonvalue = {}  
        const user = await User.findOne({username:username});
        if(!user){
            jsonvalue = {success:false, message:"User does not exist."}
        }else if(await user.validatePassword(password)){
            jsonvalue = {success:true, message: await user.toAuthJSON()}
        }else{
            jsonvalue = {success:false, message:"Wrong password."}
        }

        return jsonvalue;
    }

}

module.exports = authService;