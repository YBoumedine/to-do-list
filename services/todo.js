const mongoose = require("mongoose");
const User = require("../models/User");
const TodoList = require("../models/TodoList");

var todoManipulation = {
  insertEntry: async (_id, _title, _entry) => {
    let list = await TodoList.findOne({ user: _id, title: _title });
    if (list) {
      list.entries.push({
        id: new mongoose.Types.ObjectId(),
        entry: _entry
      });
      await list.save(err => {
        if (err) throw err;
      });

      return list;
    } else {
      return { success: false, message: "This list doesn't exist" };
    }
  },

  removeEntry: async (user_id, _title, entry_id) => {
    let list = await TodoList.findOne({ user: user_id, title: _title });
    if (list) {
      let deletion = await list.entries.id(entry_id);
      if (deletion) {
        deletion.remove();
        list.save(err => {
          if (err) throw err;
        });
        return list;
      } else {
        throw new Error("Entry does not exist");
      }
    } else {
      throw new Error("List does not exist.");
    }
  },

  createTodoList: async (_id, _title) => {
    let user = await User.findOne({ id: _id });
    let list = await TodoList.findOne({ title: _title, user: _id }).lean();
    let jsonLists = [];

    if (!user) {
      return "User does not exist.";
    }
    if (!list) {
      let newlist = new TodoList({
        user: _id,
        entries: [],
        title: _title
      });
      await newlist.save(err => {
        if (err) throw err;
      });

      return {
        listid: newlist._id,
        title: newlist.title,
        entries: newlist.entries
      };
    } else {
      return "This list already exists.";
    }
  },

  getTodos: async _id => {
    let lists = await TodoList.find({ user: _id }).lean();
    let jsonLists = [];

    if (!lists) {
      throw new Error("Could not find user.");
    } else {
      await lists.forEach(list => {
        let jsonList = {
          listid: list._id,
          title: list.title,
          entries: list.entries
        };
        jsonLists.push(jsonList);
      });
      return jsonLists;
    }
  },

  getTodoList: async (_id, _title) => {
    let list = await TodoList.findOne({ user: _id, title: _title }).lean();

    if (!list) {
      throw new Error("This list does not exist.");
    } else {
      return { entries: await list.entries };
    }
  },

  deleteAllLists: async _id => {
    let list = await TodoList.deleteMany({ user: _id });
    return { success: true, message: "Deleted all lists" };
  },

  deleteList: async (_id, _title) => {
    let list = await TodoList.findOneAndDelete({ user: _id, title: _title });

    let jsonLists = [];
    if (list) {
      let lists = await TodoList.find({ user: _id }).lean();
      await lists.forEach(list => {
        let jsonlist = {
          listid: list._id,
          title: list.title,
          entries: list.entries
        };
        jsonLists.push(jsonlist);
      });

      return jsonLists;
    } else {
      return { message: "Something happened" };
    }
  }
};

module.exports = todoManipulation;
