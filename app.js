const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const auth = require("./services/auth");
const io = require("socket.io");
const dotenv = require("dotenv");
const query = require("querystring");
const cors = require("cors");
const routes = express.Router();
const app = express();
const manipulation = require("./services/todo");

app.use(cors());
// app.use(express.static('public'))
// app.use(express.static('public/build'))
app.use("/public/build", express.static(path.join(__dirname, "/public/build")));
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log("Listening on: " + port);
});
dotenv.config();
routes.use(bodyParser.json());
routes.use(bodyParser.urlencoded({ extended: false }));

var mongoOpts = {
  useNewUrlParser: true,
  useCreateIndex: true
};

mongoose.connect(process.env.DBURL, mongoOpts);
mongoose.connection.on("error", e => {
  console.log(e);
  process.exit(1);
});

const routesToAuthentify = {
  "/test": true,
  "/me/addEntry": true,
  "/me": true,
  "/me/insertEntry": true,
  "/me/removeEntry": true,
  "/me/getList": true,
  "/me/createList": true,
  "/me/deleteAllLists": true
};

function checkAuth(req) {
  if (routesToAuthentify[req.originalUrl] && !req.user) {
    throw new Error("Unauthorized.");
  }
}

// if(process.env.NODE_ENV === 'production'){
//   //set static folder
//   app.use(express.static('client/build'));
// }

app.use("/", routes);

app.use("*", async (req, res, next) => {
  try {
    const token = req.headers.authorization;
    if (token) {
      const uservalidation = await auth.verify(token);
      req.user = uservalidation;
    }
    checkAuth(req);
    next();
  } catch (error) {
    res.send(error.toString());
  }
});

routes.post("/login", async (req, res) => {
  try {
    console.log(req.body);
    var user = await auth.login(req.body.username, req.body.password);
    res.json(user);
  } catch (error) {
    res.json(error.toString);
  }
});

routes.post("/signup", async (req, res) => {
  try {
    var creation = await auth.signup(
      req.body.email,
      req.body.username,
      req.body.password
    );
    res.send(creation);
  } catch (error) {
    res.send(error.toString());
  }
});

routes.post("/me/insertEntry", async (req, res) => {
  try {
    console.log(req.body);
    var result = await manipulation.insertEntry(
      req.body.id,
      req.body.title,
      req.body.entry
    );
    res.json(result);
  } catch (err) {
    res.send(err.toString());
  }
});

routes.delete("/me/removeEntry", async (req, res) => {
  try {
    console.log(req.query);
    var result = await manipulation.removeEntry(
      req.query.id,
      req.query.title,
      req.query.index
    );
    res.json(result);
  } catch (error) {
    throw error;
  }
});

routes.post("/me/createList", async (req, res) => {
  try {
    var result = await manipulation.createTodoList(req.body.id, req.body.title);
    res.json(result);
  } catch (error) {
    throw error;
  }
});

routes.get("/me", async (req, res) => {
  try {
    if (req.query.id) {
      var result = await manipulation.getTodos(req.query.id);
      res.json(result);
    } else {
      res.redirect("/login");
    }
  } catch (error) {
    throw error;
  }
});

routes.get("/me/getList", async (req, res) => {
  try {
    var result = await manipulation.getTodoList(req.query.id, req.query.title);
    res.json(result);
  } catch (error) {
    throw error;
  }
});

routes.delete("/me/deleteAllLists", async (req, res) => {
  try {
    var result = await manipulation.deleteAllLists(req.body.id);
    res.json(result);
  } catch (error) {
    res.send(error.toString());
    throw error;
  }
});

routes.delete("/me/deleteList", async (req, res) => {
  try {
    var result = await manipulation.deleteList(req.query.id, req.query.title);
    res.json(result);
  } catch (error) {
    res.send(error.toString());
    throw error;
  }
});

app.get("/*", async (req, res, next) => {
  res.sendFile(path.join(__dirname, "/public/build/index.html"));
});

app.use((req, res) => {
  res
    .status(404)
    .send("This method: " + req.method + " on" + req.url + " does not exist");
});
