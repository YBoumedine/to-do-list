const mongoose = require('mongoose');
const dotenv = require('dotenv');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const TodoList = require('./TodoList')

dotenv.config();


var UserSchema = mongoose.Schema({
  id:{type:mongoose.Schema.Types.ObjectId, required:true},
  email:{type:String, required:true},
  username:{type:String, required:true, unique:true},
  hashPW:{type:String, required:true},
  salt:{type:String, required:true},
})

UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hashPW = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
}

UserSchema.methods.validatePassword = function(password) {
  if (password != null) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hashPW === hash; 
  }else{
    return false;
  }
}

UserSchema.methods.generateToken = function() {
 const today = new Date();
 const expirationDate = new Date(today);
 expirationDate.setDate(today.getDate() + 60);

 return jwt.sign({
   email: this.email,
   id: this.id,
   exp: parseInt(expirationDate.getTime() / 1000, 10),
 }, process.env.SECRET);
}

UserSchema.methods.toAuthJSON = function(){
  return {
    id : this.id,
    email : this.email,
    token : this.generateToken(),
  }
}

var User = mongoose.model('User', UserSchema)

module.exports = User
