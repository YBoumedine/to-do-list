const mongoose = require('mongoose');

var TodoSchema = mongoose.Schema({
  value:String
})

var TodoListSchema = mongoose.Schema({
  user:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User',
    required:true
  },
  entries:[{
    entry:String
  }
  ],
  title:String
})

var TodoList = mongoose.model('TodoList', TodoListSchema)

module.exports = TodoList;
